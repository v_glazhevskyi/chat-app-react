import React from 'react';
import Chat from 'src/containers/Chat';
import { ReactComponent as Logo } from 'src/images/logo.svg';
import styles from './styles.module.css';

const ChatPage = () => (
  <div className={styles.container}>
    <header className={styles.header}>
      <Logo className={styles.logo} />
    </header>
    <main>
      <Chat />
    </main>
    <footer className={styles.footer}>
      <p>&#169; 2020 All Rights Reserved</p>
    </footer>
  </div>
);

export default ChatPage;

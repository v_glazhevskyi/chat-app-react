import { Action } from 'redux';

const initialState = {
  user: {
    id: '9e243930-83c9-11e9-8e0c-8f1a686f4ce4',
    name: 'Ruth',
    avatar:
      'https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA',
  } as IUser,
  chat: {
    title: 'My very own chat',
  },
};

export default (state = initialState, action: Action) => {
  return { ...state };
};

import actionTypes from '../actionTypes';
import { getMessagesSuccess } from '../actions';

type Action = ReturnType<typeof getMessagesSuccess>;

type UserDict = { [x: string]: IUser };

const select = (item: IRawMessage) => {
  const { userId, user, avatar } = item;
  return {
    id: userId,
    name: user,
    avatar,
  };
};

const byId = (users: IUser[]) => users.reduce((acc, item) => ({ ...acc, [item.id]: item }), {} as UserDict);

export default (state: UserDict = {}, { type, payload }: Action) => {
  switch (type) {
    case actionTypes.GET_MESSAGES_SUCCESS:
      return { ...state, ...byId(payload.messages.map(select)) };
    default:
      return state;
  }
};

import actions from '../actionTypes';
import { getMessages, getMessagesSuccess, getMessagesFailure } from '../actions';

type Action =
  | ReturnType<typeof getMessages>
  | ReturnType<typeof getMessagesSuccess>
  | ReturnType<typeof getMessagesFailure>;

const initialState = {
  fetchingMessages: false,
};

export default (state = initialState, { type }: Action) => {
  switch (type) {
    case actions.GET_MESSAGES:
      return { ...state, fetchingMessages: true };
    case actions.GET_MESSAGES_SUCCESS:
    case actions.GET_MESSAGES_FAILURE:
      return { ...state, fetchingMessages: false };
    default:
      return state;
  }
};

import actionTypes from '../actionTypes';
import { toggleLike } from '../actions';

type Action = ReturnType<typeof toggleLike>;

const toggle = (key: string, list: string[]) => (list.includes(key) ? list.filter((k) => k === key) : [...list, key]);

export default (state: string[] = [], { type, payload }: Action) => {
  switch (type) {
    case actionTypes.TOGGLE_LIKE:
      return toggle(payload.messageId, state);
    default:
      return state;
  }
};

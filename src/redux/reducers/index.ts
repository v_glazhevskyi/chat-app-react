import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import profile from './profileReducer';
import messageById from './messageReducer';
import userById from './userReducer';
import fetch from './fetchReducer';
import myLikes from './myLikesReducer';

const createRootReducer = (history: any) =>
  combineReducers({
    router: connectRouter(history),
    profile,
    messageById,
    userById,
    myLikes,
    fetch,
  });

export default createRootReducer;

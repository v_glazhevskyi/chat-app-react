import actionTypes from '../actionTypes';
import { getMessagesSuccess, toggleLike, updateMessage, deleteMessage } from '../actions';

type Action = ReturnType<typeof getMessagesSuccess> &
  ReturnType<typeof toggleLike> &
  ReturnType<typeof updateMessage> &
  ReturnType<typeof deleteMessage>;

type MessageDict = { [id: string]: IMessage };

const select = (item: IRawMessage) => {
  const { id, userId, text, createdAt, editedAt, likeCount } = item;
  return {
    id,
    userId,
    text,
    createdAt,
    editedAt,
    likeCount,
  };
};

const byId = (messages: IMessage[]) => messages.reduce((acc, item) => ({ ...acc, [item.id]: item }), {} as MessageDict);

const incrementLikeCount = (messageId: string, value: number, state: MessageDict) => {
  const message = state[messageId];
  const likeCount = message.likeCount + value;
  return { ...state, [messageId]: { ...message, likeCount } };
};

export default (state: MessageDict = {}, { type, payload }: Action) => {
  switch (type) {
    case actionTypes.GET_MESSAGES_SUCCESS:
      return {
        ...state,
        ...byId(payload.messages.map(select)),
      };

    case actionTypes.TOGGLE_LIKE:
      return incrementLikeCount(payload.messageId, payload.value, state);

    case actionTypes.SEND_MESSAGE:
    case actionTypes.UPDATE_MESSAGE:
      return {
        ...state,
        [payload.message.id]: payload.message,
      };

    case actionTypes.DELETE_MESSAGE:
      return byId(
        Object.keys(state)
          .filter((id) => id !== payload.messageId)
          .map((id) => state[id]),
      );

    default:
      return state;
  }
};

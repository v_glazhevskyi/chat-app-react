import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import { createBrowserHistory } from 'history';
import createRootReducer from './reducers';
import mySaga from './sagas';

const sagaMiddleware = createSagaMiddleware();
const middlewares = [sagaMiddleware];

const composedEnhancers = composeWithDevTools(applyMiddleware(...middlewares));

export const history = createBrowserHistory();
const rootReducer = createRootReducer(history);

export type RootState = ReturnType<typeof rootReducer>;

const configureStore = () => {
  const store = createStore(rootReducer, composedEnhancers);
  sagaMiddleware.run(mySaga);
  return store;
};

export default configureStore;

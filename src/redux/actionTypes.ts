const GET_MESSAGES = 'MESSAGES/REQUEST';
const GET_MESSAGES_SUCCESS = 'MESSAGES/REQUEST/SUCCESS';
const GET_MESSAGES_FAILURE = 'MESSAGES/REQUEST/FAILURE';

const TOGGLE_LIKE = 'TOGGLE_LIKE';
const SEND_MESSAGE = 'MESSAGE/CREATE';
const UPDATE_MESSAGE = 'MESSAGE/UPDATE';
const DELETE_MESSAGE = 'MESSAGE/DELETE';

export default {
  GET_MESSAGES,
  GET_MESSAGES_SUCCESS,
  GET_MESSAGES_FAILURE,
  TOGGLE_LIKE,
  SEND_MESSAGE,
  UPDATE_MESSAGE,
  DELETE_MESSAGE,
};

import { call, put, takeLatest } from 'redux-saga/effects';
import chatService from 'src/services/chatService';
import actionTypes from '../actionTypes';
import { getMessagesSuccess, getMessagesFailure } from '../actions';

function* fetchMessages(action: any) {
  try {
    const messages = yield call(chatService.getMessages);
    yield put(getMessagesSuccess(messages));
  } catch (error) {
    yield put(getMessagesFailure(error));
  }
}

export default function* () {
  yield takeLatest(actionTypes.GET_MESSAGES, fetchMessages);
}

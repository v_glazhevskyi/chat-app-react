import { v4 as uuidv4 } from 'uuid';
import actionTypes from './actionTypes';

export const getMessages = () => ({
  type: actionTypes.GET_MESSAGES,
});

export const getMessagesSuccess = (messages: IRawMessage[]) => ({
  type: actionTypes.GET_MESSAGES_SUCCESS,
  payload: { messages },
});

export const getMessagesFailure = (error: Error) => ({
  type: actionTypes.GET_MESSAGES_FAILURE,
  payload: { error },
});

export const toggleLike = (message: IChatMessage) => ({
  type: actionTypes.TOGGLE_LIKE,
  payload: {
    messageId: message.id,
    value: message.likedByMe ? -1 : 1,
  },
});

export const updateMessage = (message: IChatMessage) => ({
  type: actionTypes.UPDATE_MESSAGE,
  payload: { message },
});

export const deleteMessage = (message: IChatMessage) => ({
  type: actionTypes.DELETE_MESSAGE,
  payload: { messageId: message.id },
});

export const sendMessage = (text: string, userId: string) => ({
  type: actionTypes.SEND_MESSAGE,
  payload: {
    message: {
      id: uuidv4(),
      text,
      userId,
      createdAt: new Date().toISOString(),
      editedAt: '',
      likeCount: 0,
    },
  },
});

interface IRawMessage {
  id: string;
  userId: string;
  avatar: string;
  user: string;
  text: string;
  createdAt: string;
  editedAt: string;
  likeCount: number;
}

interface IMessage {
  id: string;
  userId: string;
  text: string;
  createdAt: string;
  editedAt: string;
  likeCount: number;
}

type IChatMessage = IMessage & {
  author: IUser;
  itsOwn: boolean;
  likedByMe: boolean;
};

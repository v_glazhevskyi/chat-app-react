import React, { useState } from 'react';
import { Button, Modal, Form } from 'semantic-ui-react';

type Props = {
  message: IChatMessage | null;
  onClose: () => void;
  onSuccess: (message: IChatMessage) => void;
};

const EditMessageModal = ({ message, onClose, onSuccess }: Props) => {
  const [text, setText] = useState(message?.text);

  const handleSubmit = () => {
    if (!message || !text) {
      return;
    }
    onSuccess({ ...message, text });
    setText('');
  };

  return (
    <Modal dimmer="blurring" size="tiny" open={Boolean(message)} onClose={onClose}>
      <Modal.Header>Edit Message</Modal.Header>
      <Modal.Content>
        <Form>
          <Form.TextArea
            name="text"
            value={text || message?.text} // useState not works as expected for some reason
            onChange={(e) => setText(e.target.value)}
            placeholder="Write something"
          />
        </Form>
      </Modal.Content>
      <Modal.Actions>
        <Button negative onClick={onClose} content="Cancel" />
        <Button
          positive
          onClick={handleSubmit}
          disabled={!text?.length}
          icon="send"
          labelPosition="right"
          content="Update"
        />
      </Modal.Actions>
    </Modal>
  );
};

export default EditMessageModal;

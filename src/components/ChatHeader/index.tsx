import React from 'react';
import { Header, Popup } from 'semantic-ui-react';
import { formatDate, timesAgo } from 'src/utils/dateUtils';
import styles from './styles.module.css';

type Props = {
  chatTitle: string;
  usersCount: number;
  messagesCount: number;
  lastMessageDate: Date;
};

const ChatHeader = ({ chatTitle, usersCount, messagesCount, lastMessageDate }: Props) => (
  <Header size="large" attached="top" className={styles.container}>
    <section className={styles.left}>
      <p className={styles.infoItem}>{chatTitle}</p>
      <p className={styles.infoItem}>{usersCount} participants</p>
      <p className={styles.infoItem}>{messagesCount} messages</p>
    </section>
    <section className={styles.right}>
      <Popup
        content={formatDate(lastMessageDate)}
        trigger={<p className={styles.aside}>Last message: {timesAgo(lastMessageDate)}</p>}
      />
    </section>
  </Header>
);

export default ChatHeader;

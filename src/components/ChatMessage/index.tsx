import React from 'react';
import GeneralMessage from './GeneralMessage';
import OwnMessage from './OwnMessage';

type Props = {
  message: IChatMessage;
  onLike: () => void;
  onEdit: () => void;
  onDelete: () => void;
};

const ChatMessage = ({ message, onLike, onEdit, onDelete }: Props) =>
  message.itsOwn ? (
    <OwnMessage message={message} onEdit={onEdit} onDelete={onDelete} />
  ) : (
    <GeneralMessage message={message} onLike={onLike} />
  );

export default ChatMessage;

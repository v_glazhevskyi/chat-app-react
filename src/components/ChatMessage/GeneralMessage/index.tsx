import React from 'react';
import { Feed, Icon, Popup } from 'semantic-ui-react';
import { formatDate, timesAgo } from 'src/utils/dateUtils';
import styles from './styles.module.css';

type Props = {
  message: IChatMessage;
  onLike: () => void;
};

const GeneralMessage = ({ message, onLike }: Props) => (
  <Feed.Event>
    <Feed.Label image={message.author.avatar} />
    <Feed.Content className={`${styles.content}`}>
      <Feed.Summary>
        <a href="/">{message.author.name}</a>
        <Popup content={formatDate(message.createdAt)} trigger={<Feed.Date>{timesAgo(message.createdAt)}</Feed.Date>} />
      </Feed.Summary>
      <Feed.Extra text>{message.text}</Feed.Extra>
      <Feed.Meta>
        <Feed.Like onClick={onLike}>
          <Icon name="like" className={message.likedByMe ? styles.iconActive : ''} />
          {message.likeCount} Likes
        </Feed.Like>
      </Feed.Meta>
    </Feed.Content>
  </Feed.Event>
);

export default GeneralMessage;

import React, { useState } from 'react';
import { Feed, Icon, Popup } from 'semantic-ui-react';
import { formatDate, timesAgo } from 'src/utils/dateUtils';
import styles from './styles.module.css';

type Props = {
  message: IMessage;
  onEdit: () => void;
  onDelete: () => void;
};

const OwnMessage = ({ message, onEdit, onDelete }: Props) => {
  const [ctlVisible, setCtlVisible] = useState(false);
  const hiddenClass = !ctlVisible ? styles.hidden : '';

  return (
    <Feed.Event className={styles.event}>
      <Feed.Content
        className={styles.content}
        onMouseOver={() => setCtlVisible(true)}
        onMouseOut={() => setCtlVisible(false)}
        onFocus={() => setCtlVisible(true)}
        onBlur={() => setCtlVisible(false)}
      >
        <Feed.Summary>
          <a href="/">I am</a>
          <Popup
            content={formatDate(message.createdAt)}
            trigger={<Feed.Date>{timesAgo(message.createdAt)}</Feed.Date>}
          />
        </Feed.Summary>
        <Feed.Extra text>{message.text}</Feed.Extra>
        <Feed.Meta className={styles.meta}>
          <Feed.Label>
            <Icon name="like" disabled />
            {message.likeCount} Likes
          </Feed.Label>
          <Feed.Label className={hiddenClass}>
            <button className={styles.btnEdit} type="button" onClick={onEdit}>
              <Icon name="edit" />
              Edit Message
            </button>
          </Feed.Label>
          <Feed.Label className={hiddenClass}>
            <button className={styles.btnDel} type="button" onClick={onDelete}>
              <Icon name="delete" />
              Delete
            </button>
          </Feed.Label>
        </Feed.Meta>
      </Feed.Content>
    </Feed.Event>
  );
};

export default OwnMessage;

import React, { useState } from 'react';
import { Segment, Form, TextArea, Button } from 'semantic-ui-react';
import styles from './styles.module.css';

type ChatMessageInputProps = {
  onSend: (text: string) => void;
  onEdit: () => IChatMessage;
  onEditSuccess: (text: string) => void;
};

const ChatMessageInput = ({ onSend, onEdit, onEditSuccess }: ChatMessageInputProps) => {
  const [text, setText] = useState('');
  const [editMode, setEditMode] = useState(false);

  const handleSubmit = () => {
    if (editMode) {
      onEditSuccess(text);
    } else {
      onSend(text);
    }
    setEditMode(false);
    setText('');
  };

  const keyEventHandler = (e: React.KeyboardEvent) => {
    if (e.code !== 'ArrowUp') {
      return;
    }
    const message = onEdit();
    setText(message.text);
    setEditMode(true);
  };

  return (
    <Form onSubmit={handleSubmit}>
      <Segment className={styles.container}>
        <TextArea
          name="text"
          className={styles.textArea}
          rows={2}
          placeholder="Write something!"
          onChange={(e) => setText(e.target.value)}
          onKeyDown={keyEventHandler}
          value={text}
        />
        <Button
          className={styles.submitBtn}
          icon="send"
          positive={editMode}
          labelPosition="right"
          content={editMode ? 'Update' : 'Send'}
          disabled={!text.length}
          type="submit"
        />
      </Segment>
    </Form>
  );
};

export default ChatMessageInput;

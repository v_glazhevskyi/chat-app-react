import React from 'react';
import './styles.module.css';

type Props = {
  children: JSX.Element;
  title: string;
};

const ChatMessageGroup = ({ children, title }: Props) => {
  return (
    <>
      <hr title={title} />
      <div className="container">{children}</div>
    </>
  );
};

export default ChatMessageGroup;

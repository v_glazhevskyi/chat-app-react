import React from 'react';
import { Feed } from 'semantic-ui-react';
import groupBy from 'src/utils/groupBy';
import { timesAgo } from 'src/utils/dateUtils';
import ChatMessage from '../ChatMessage';
import ChatMessageGroup from '../ChatMessageGroup';
import styles from './styles.module.css';

type Props = {
  messages: IChatMessage[];
  onLike: (message: IChatMessage) => void;
  onEdit: (message: IChatMessage) => void;
  onDelete: (message: IChatMessage) => void;
};

const ChatMessageList = ({ messages, onLike, onEdit, onDelete }: Props) => {
  const messagesByGroup = messages.reduce(
    groupBy((item) => timesAgo(item.createdAt, true)),
    {},
  );

  const renderGroup = (title: string, list: IChatMessage[]) => (
    <ChatMessageGroup title={title}>
      <Feed className={styles.feedContainer}>
        {list.map((message) => (
          <ChatMessage
            key={message.id}
            message={message}
            onLike={() => onLike(message)}
            onEdit={() => onEdit(message)}
            onDelete={() => onDelete(message)}
          />
        ))}
      </Feed>
    </ChatMessageGroup>
  );

  return (
    <div className={styles.container}>
      {Object.keys(messagesByGroup).map((title) => renderGroup(title, messagesByGroup[title]))}
    </div>
  );
};

export default ChatMessageList;

import React, { useEffect, useState } from 'react';
import { Confirm } from 'semantic-ui-react';
import Spinner from 'src/components/Spinner';
import ChatHeader from 'src/components/ChatHeader';
import ChatMessageList from 'src/components/ChatMessageList';
import ChatMessageInput from 'src/components/ChatMessageInput';
import EditMessageModal from 'src/components/EditMessageModal';
import { getUniqueUserIds, getLastMessageDate } from './helpers';

type Props = {
  chatTitle: string;
  currentUser: {
    id: string;
    name: string;
    avatar: string;
  };
  messages: IChatMessage[];
  getMessages: () => void;
  toggleLike: (message: IChatMessage) => void;
  sendMessage: (text: string, userId: string) => void;
  updateMessage: (message: IChatMessage) => void;
  deleteMessage: (message: IChatMessage) => void;
  fetchingMessages: boolean;
};

const Chat = ({
  chatTitle,
  currentUser,
  messages,
  getMessages,
  toggleLike,
  sendMessage,
  updateMessage,
  deleteMessage,
  fetchingMessages,
}: Props) => {
  useEffect(() => getMessages(), [getMessages]);

  const [editingMessage, setEditingMessage] = useState<IChatMessage | null>(null);
  const [deletingMessage, setDeletingMessage] = useState<IChatMessage | null>(null);

  if (!messages.length || fetchingMessages) {
    return <Spinner />;
  }

  const userIds = getUniqueUserIds(messages);
  const lastMessageDate = getLastMessageDate(messages);
  const [lastOwnMessage] = messages.filter((item) => item.itsOwn).reverse();

  const onSendMessage = (text: string) => sendMessage(text, currentUser.id);
  const getLastOwnMessage = () => lastOwnMessage;
  const onEditLastMessageSuccess = (text: string) => updateMessage({ ...lastOwnMessage, text });

  const onEditMessage = (message: IChatMessage) => setEditingMessage(message);
  const onCancelEdit = () => setEditingMessage(null);
  const onSuccessEditing = (message: IChatMessage) => {
    setEditingMessage(null);
    updateMessage(message);
  };

  const onDeleteMessage = (message: IChatMessage) => setDeletingMessage(message);
  const onCancelDelete = () => setDeletingMessage(null);
  const onConfirmDelete = () => {
    if (!deletingMessage) {
      return;
    }
    deleteMessage(deletingMessage);
    setDeletingMessage(null);
  };

  return (
    <>
      <ChatHeader
        chatTitle={chatTitle}
        usersCount={userIds.length}
        messagesCount={messages.length}
        lastMessageDate={lastMessageDate}
      />
      <ChatMessageList messages={messages} onLike={toggleLike} onEdit={onEditMessage} onDelete={onDeleteMessage} />
      <ChatMessageInput onSend={onSendMessage} onEdit={getLastOwnMessage} onEditSuccess={onEditLastMessageSuccess} />
      <EditMessageModal message={editingMessage} onClose={onCancelEdit} onSuccess={onSuccessEditing} />
      <Confirm
        dimmer="blurring"
        size="tiny"
        open={Boolean(deletingMessage)}
        onCancel={onCancelDelete}
        onConfirm={onConfirmDelete}
      />
    </>
  );
};

export default Chat;

import unique from 'src/utils/unique';

export const getUniqueUserIds = (messages: IMessage[]) => {
  const userIds = messages.map((item) => item.userId).sort();
  return unique(userIds);
};

export const getLastMessageDate = (messages: IMessage[]) => {
  const [lastDate] = messages
    .map((item) => new Date(item.createdAt))
    .sort()
    .slice(-1);
  return lastDate;
};

import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';
import { RootState } from 'src/redux/configureStore';
import { getMessages, toggleLike, sendMessage, updateMessage, deleteMessage } from 'src/redux/actions';
import Chat from './Chat';

const messagesSelector = (state: RootState) => {
  const { messageById, userById, myLikes, profile } = state;
  const currentUserId = profile.user.id;

  return Object.keys(messageById)
    .map((id) => {
      const message = messageById[id];
      return {
        ...message,
        author: userById[message.userId],
        itsOwn: message.userId === currentUserId,
        likedByMe: myLikes.includes(message.id),
      };
    })
    .sort((a, b) => new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime());
};

const mapState = (state: RootState) => ({
  currentUser: state.profile.user,
  chatTitle: state.profile.chat.title,
  messages: messagesSelector(state),
  fetchingMessages: state.fetch.fetchingMessages,
  myLikes: state.myLikes,
});

const actions = {
  getMessages,
  toggleLike,
  sendMessage,
  updateMessage,
  deleteMessage,
};

const mapDispatch = (dispatch: Dispatch) => bindActionCreators(actions, dispatch);

const connector = connect(mapState, mapDispatch);

export default connector(Chat);

type Item = IChatMessage;
type KeyMakerFunction = (item: Item) => string;
type Accumulator = { readonly [key: string]: Item[] };

const groupBy = (fn: KeyMakerFunction) => (acc: Accumulator, item: Item) => {
  const key = fn(item);
  if (!acc) {
    return { [key]: [item] };
  }
  const group = acc[key];
  if (!group) {
    return { ...acc, [key]: [item] };
  }
  return { ...acc, [key]: [...group, item] };
};

export default groupBy;

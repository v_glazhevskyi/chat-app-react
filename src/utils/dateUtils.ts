const SECOND = 1000;
const MINUTE = 60 * SECOND;
const HOUR = 60 * MINUTE;
const DAY = 24 * HOUR;
const WEEK = 7 * DAY;
const MONTH = 30 * DAY;
const YEAR = 365 * DAY;

export const splitDate = (dateObj: Date) => {
  const isoString = dateObj.toISOString();
  const [date, rest] = isoString.split('T');
  const [yyyy, MM, dd] = date.split('-');
  const [hh, mm, subminutes] = rest.split(':');
  const [ss, ms] = subminutes.split('.');
  const time = `${hh}:${mm}:${ss}`;
  return { date, time, yyyy, MM, dd, hh, mm, ss, ms };
};

export const getDiff = (dateA: Date, dateB: Date) => {
  return Math.abs(dateA.getTime() - dateB.getTime());
};

export const formatDate = (maybeDate: string | Date) => {
  const { date, time } = splitDate(new Date(maybeDate));
  return `${date} ${time}`;
};

export const timesAgo = (maybeDate: string | Date, approx = false) => {
  const date = new Date(maybeDate);
  const now = new Date();

  const { hh, mm } = splitDate(date);

  const milliseconds = getDiff(now, date);
  const minutes = Math.floor(milliseconds / MINUTE);
  const hours = Math.floor(milliseconds / HOUR);
  const days = Math.floor(milliseconds / DAY);
  const weeks = Math.floor(milliseconds / WEEK);
  const monthes = Math.floor(milliseconds / MONTH);
  const years = Math.floor(milliseconds / YEAR);

  if (minutes < 5 && !approx) {
    return 'Now';
  }
  if (minutes < 60 && !approx) {
    return `${minutes} minutes ago`;
  }
  if (hours < 6 && !approx) {
    return `${hours} hours ago`;
  }
  if (days === 0 && !approx) {
    return `Today at ${hh}:${mm}`;
  }
  if (days === 0) {
    return `Today`;
  }
  if (days === 1) {
    return 'Yesterday';
  }
  if (weeks === 0) {
    return `${days} days ago`;
  }
  if (monthes === 0) {
    return `${weeks} weeks ago`;
  }
  if (years === 0) {
    return `${monthes} months ago`;
  }
  return `${years} years ago`;
};

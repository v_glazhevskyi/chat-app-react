const unique = (list: string[]) =>
  list.sort().reduce((acc: string[], item) => {
    const [head] = acc.slice(-1);
    if (!head) {
      return [item];
    }
    if (head === item) {
      return acc;
    }
    return [...acc, item];
  }, []);

export default unique;

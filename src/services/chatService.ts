import { v4 as uuidv4 } from 'uuid';
import delay from 'src/utils/delay';
import { HOST } from 'src/config';

const getFakeLikes = () => Math.floor(Math.random() * 4);

const getMessages = async (): Promise<IRawMessage[]> => {
  const url = `${HOST}/messages.json`;
  const response = await fetch(url);
  await delay(1000);
  const messages: IRawMessage[] = await response.json();

  // Fix message id, because it's the same in whole messages
  return messages.map((item) => ({
    ...item,
    id: uuidv4(),
    likeCount: getFakeLikes(),
  }));
};

export default { getMessages };

import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import ChatPage from './pages/ChatPage';

function App() {
  return (
    <Switch>
      <Route exact path="/" render={() => <Redirect to="/chat" />} />
      <Route path="/chat" component={ChatPage} />
      <Route path="/" render={() => <h1>404 Not Found</h1>} />
    </Switch>
  );
}

export default App;
